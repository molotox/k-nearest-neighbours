# this program calculates the K nearest neighbours to a point in R^3 space

import xml.etree.ElementTree as et
import math


def main():
    # extracts data from the associated XML file into a tree
    point_tree = et.parse('points.xml')
    point_tree = point_tree.getroot()
    node_list = []

    # parses data from the tree and makes a list of nodes
    for point in point_tree:
        c_name = point.get("name")
        c_state = point.get("state")
        coordinates = point.getchildren()

        x_val = coordinates[0].text
        y_val = coordinates[1].text
        z_val = coordinates[2].text

        current = Node(c_name, x_val, y_val, z_val)
        current.state = (c_state == "yes")
        node_list.append(current)

    i = 0
    for node in node_list:
        print(i, node.to_string())
        i += 1

    print()
    print("Here we will define a point in R^3 space to be classified")
    print()
    x_val = float(input("Enter the x coordinate"))
    y_val = float(input("Enter the y coordinate"))
    z_val = float(input("Enter the z coordinate"))

    # pivot represents a point in R^3 space that will be classified as yes/no based on the state of its neighbours.
    pivot = Node("Pivot", x_val, y_val, z_val)

    pivot.distance_to_pivot = 0

    print()
    print("The implementation of Knn allows the user to choose between three different measures of distance.")
    print("0: Euclidean")
    print("1: Manhattan")
    print("N: Chebyschev")
    print()
    # the modulus of the choice is taken to ensure that teh choice is valid for all inputs
    choice = (int(input("Please enter a number from 0, to 2"))) % 3  # type: int

    pivot.dist_type = choice

    # this code block calculates the distance of each point from the pivot point
    for node in node_list:
        node.distance_to_pivot = pivot.distance(node)

    neighbours = sorted(node_list, key=lambda Node: Node.distance_to_pivot)

    print()
    K = int(input("Please enter a value for K"))

    if K > len(neighbours):
        K = len(neighbours)

    neighbours = neighbours[0:K]

    i = 0
    for point in neighbours:
        print(i, point.to_string(), "Distance: ", point.distance_to_pivot)
        i += 1


    weight = 0
    for point in neighbours:
        if point.state:
            weight += 1
        else:
            weight += -1

    if weight == 0:
        print("Test Inconclusive")
    elif weight > 0:
        print("Prediction: State = yes")
    else:
        print("Prediction: State = no")


# Class definition for a point in R^3
class Node:

    def __init__(self, name, x, y, z):
        self.state = None
        self.name = name
        self.x_val = float(x)
        self.y_val = float(y)
        self.z_val = float(z)
        self.distance_to_pivot = 0
        self.dist_type = 0

    # Formula for Euclidean distance (pythagoras formula)
    def euclid_dist(self, point):
        distance = (self.x_val - point.x_val) ** 2
        distance += (self.y_val - point.y_val) ** 2
        distance += (self.z_val - point.z_val) ** 2

        distance = math.sqrt(distance)  # type: float

        return distance

    # Formula for Manhattan distance
    def manhattan_dist(self, point):
        distance = abs(self.x_val - point.x_val)
        distance += abs(self.y_val - point.y_val)
        distance += abs(self.z_val - point.z_val)

        return distance

    # Formula for Chebyschev distance
    def cheb_dist(self, point):
        distances = [abs(self.x_val - point.x_val), abs(self.y_val - point.y_val), abs(self.z_val - point.z_val)]

        return max(distances)

    def distance(self, point):
        if self.dist_type == 0:
            return self.euclid_dist(point)
        elif self.dist_type == 1:
            return self.manhattan_dist(point)
        else:
            return self.cheb_dist(point)

    # returns a string containing the fields of the Node
    def to_string(self):
        return self.name + ": (" + str(self.x_val) + "," + str(self.y_val) + "," + str(self.z_val) + "), State: " + str(self.state)


if __name__ == '__main__':
    main()
